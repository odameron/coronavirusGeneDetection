#! /usr/bin/env python3

import random

#taxon = "Escherichia_coli"
#taxon = "Escherichia_coli_SMALL"
#taxon = "Escherichia_coli_MINI25"
taxon = "MN908947.3" # coronavirus

#readLength = 100
readLength = 20

#maxOffset = 5
maxOffset = 1

genomeSeq = ""

with open(taxon + ".fasta") as genomeFile:
	for currentLine in genomeFile:
		if currentLine.startswith(">"):
			continue
		genomeSeq += currentLine.replace("\n", "")

print(genomeSeq)
print()

genomeLength = len(genomeSeq)
print("Genome length: {}".format(genomeLength))

readIndex = 0
with open(taxon + "_READS.fasta", "w") as readsFile:
	while readIndex+readLength-maxOffset < genomeLength:
		readsFile.write(">read" + str(readIndex) + " " + taxon + "\n")
		readsFile.write(genomeSeq[readIndex:min(readIndex+readLength,genomeLength)] + "\n")
		readIndex += random.randint(1,maxOffset)
