#! /usr/bin/env python3

import random

#taxon = "Escherichia_coli"
#taxon = "Escherichia_coli_SMALL"
#taxon = "Escherichia_coli_MINI25"
taxon = "MN908947.3" # coronavirus

genomeSeq = ""

with open(taxon + ".fasta") as genomeFile:
	for currentLine in genomeFile:
		if currentLine.startswith(">"):
			continue
		genomeSeq += currentLine.replace("\n", "")

print(genomeSeq)
print()

genomeLength = len(genomeSeq)
print("Genome length: {}".format(genomeLength))

