#! /usr/bin/env python3

import random

#taxon = "Escherichia_coli"
#taxon = "Escherichia_coli_SMALL"
#taxon = "Escherichia_coli_MINI25"
taxon = "MN908947.3" # coronavirus

genomeSeq = ""

with open(taxon + ".fasta") as genomeFile:
    for currentLine in genomeFile:
        if currentLine.startswith(">"):
            continue
        genomeSeq += currentLine.replace("\n", "")

print(genomeSeq)
print()

genomeLength = len(genomeSeq)
print("Genome length: {}".format(genomeLength))

nbStarts = 0
nbGenes = 0
for i in range(genomeLength - 3):
    if genomeSeq[i:i+3] == "ATG":
        nbStarts += 1
    #if genomeSeq[i:i+3] == "TAC":
        #j = i + 3
        j = i + 300
        while (j < genomeLength - 3) and (j-i < 4500) and (genomeSeq[j:j+3] not in ["TAA", "TAG", "TGA"]):
            j += 3
        if genomeSeq[j:j+3] in ["TAA", "TAG", "TGA"]:
        #if genomeSeq[j:j+3] in ["ATT", "ATC", "ACT"]:
            #print("{}\t{}\t{}".format(i, j, j-i))
            nbGenes += 1
print()
print("Nb START: {}".format(nbStarts))
print("Nb genes: {}".format(nbGenes))

            