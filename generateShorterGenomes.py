#! /usr/bin/env python3

#taxon = "Escherichia_coli"
#taxon = "Escherichia_coli_SMALL"
#taxon = "Escherichia_coli_MINI25"
taxon = "MN908947.3" # coronavirus


genomeSeq = ""

with open(taxon + ".fasta") as genomeFile:
	for currentLine in genomeFile:
		if currentLine.startswith(">"):
			continue
		genomeSeq += currentLine.replace("\n", "")

#print(genomeSeq)
print()

genomeLength = len(genomeSeq)
print("Genome length: {}".format(genomeLength) )

#readIndex = 0
#with open(taxon + "_READS.fasta", "w") as readsFile:
#	while readIndex+readLength-maxOffset < genomeLength:
#		readsFile.write(">read" + str(readIndex) + " " + taxon + "\n")
#		readsFile.write(genomeSeq[readIndex:min(readIndex+readLength,genomeLength)] + "\n")
#		readIndex += random.randint(1,maxOffset)

lineLength = 80
for fractionLength in [100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000]:
	if fractionLength > genomeLength:
		continue
	with open(taxon + "_fraction" + str(fractionLength).zfill(7) + ".fasta", "w") as fractionGenomeFile:
		fractionGenomeFile.write(">Fraction of the genome of " + taxon + ": first " + str(fractionLength) + "\n")
		startPos = 0
		while startPos < min(fractionLength, genomeLength):
			fractionGenomeFile.write(genomeSeq[startPos:min(fractionLength, genomeLength, startPos+lineLength)] + "\n")
			startPos += lineLength
