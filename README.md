# quels sont les gènes candidats pour le coronavirus SARS-cov2 ?


à partir du fichier joint `MN908947.3.fasta` (le genome du coronavirus) et en s'inspirant du programme qui reconstitue toute la séquence ADN

- étude de la séquence
	- quelle est sa longueur ?
	- est-ce qu'il y a des nucléotides plus présents que d'autres ? combien y a-t-il de A, de C, de G et de T ?
	- est-ce que les nucléotides sont uniforméments répartis tout au long de la séquence, ou bien y en a-t-il que l'on retrouve préférentiellement aux extrémités, ou au milieu ? 
		- Faites des boxplots des positions moyennes
		- Faites des fenêtres glissantes
	- est-ce qu'il y a des enchaînements de nucléotides plus fréquents que d'autres ?
	- comment généraliser à des enchaînements de suites de nucléotides ?
- recherche des gènes candidats
	- combien y a-t-il de codons start (ATG) ?
	- à partir de chaque codon start, chercher le premier codon stop (TAA, TAG, TGA) qui soit séparé du start par un nombre entier de codons et pas trop loin (on va dire entre 300 et 4500 bases, soit entre 100 et 1500 AA)
	- combien y a-t-il de gènes candidats ?
	- pour chaque gène candidat afficher sa position de début (le début du start), sa position de fin (le début du stop), sa longueur et sa séquence 
	- y a-t-il des gènes inclus l'un dans l'autre ?
	- y a-t-il des gènes qui se chevauchent ?
- étude de la localisation des gènes candidats
	- est-ce que les gènes sont répartis uniformément tout au long de la séquence ?

